**com.sedion.basic.util内为基本信息工具：**

1.SendBasicService为自动回复6大类消息的封装
 

**com.sedion.menu.util内为菜单管理工具：**

1.MenuUtil为增删差自定义菜单的封装

2.MenuManager为自定义菜单的内容类型管理工具（设置菜单显示的样式和作用）


**com.sedion.wechatapi.util内为基本的工具：**

1.CommonUtil内包括 发起https请求并获取结果工具、获取access_token工具、URL编码(utf-8)工具、根据类型判断文件扩展名工具

2.MyX509TrustManager为证书信任管理器

3.SignUtil 为验证消息真实性(请求来源于微信)工具类  

4.WeixinUtil包括了接收和发送消息类型初始化以及微信公众平台接口Url的初始化

5.XmlMessUtil是xml形式消息处理工具类


**com.sedion.advanced.util内为高级接口的封装**

具体使用方法请查看WIKI [http://git.oschina.net/codenewbie/wechatbasic/wikis/home](http://git.oschina.net/codenewbie/wechatbasic/wikis/home)